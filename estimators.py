from abc import ABC, abstractmethod
import numpy as np

# import utils_space


class Estimator(ABC):

    @abstractmethod
    def estimate(self, *args, **kwargs):
        pass

    def __call__(self, *args, **kwargs):
        return self.estimate(*args, **kwargs)


class QEstimator(Estimator):

    def __init__(self, estimator, extractor):
        self.estimator = estimator
        self.extractor = extractor

    def estimate(self, observation, action):
        features = self.extractor(observation, action)
        return self.estimator(features)

    def get_model(self):
        return self.estimator.get_model()

    def get_max(self):
        return self.estimator.get_model()[0, 0].max()

    def reset_model(self):
        self.estimator.reset_model()


class DoubleQEstimator(Estimator):

    def __init__(self, estimator, extractor):
        self.estimator1, self.estimator2 = estimator
        self.extractor = extractor

    def estimate(self, observation, action):
        features = self.extractor(observation, action)
        return (self.estimator1(features) + self.estimator2(features)) / 2

    def get_model(self):
        return self.estimator1.get_model(), self.estimator2.get_model()

    def get_max(self):
        mean = (self.estimator1.get_model()[0, 0] + self.estimator2.get_model()[0, 0]) / 2
        return np.max(mean)

    def reset_model(self):
        self.estimator1.reset_model()
        self.estimator2.reset_model()


class ClippedDoubleQEstimator(Estimator):

    def __init__(self, estimator, extractor):
        self.estimator1, self.estimator2 = estimator
        self.extractor = extractor

    def estimate(self, observation, action):
        features = self.extractor(observation, action)
        return np.min([self.estimator1(features),
                        self.estimator2(features)], axis=0)

    def get_model(self):
        return self.estimator1.get_model(), self.estimator2.get_model()

    def get_max(self):
        mean = (self.estimator1.get_model()[0, 0] + self.estimator2.get_model()[0, 0]) / 2
        return np.max(mean)

    def reset_model(self):
        self.estimator1.reset_model()
        self.estimator2.reset_model()


class Q2QEstimator(Estimator):

    def __init__(self, estimator, extractor):
        self.estimator, self.estimator1, self.estimator2 = estimator
        self.extractor = extractor

    def estimate(self, observation, action):
        features = self.extractor(observation, action)
        return .5*self.estimator(features) + .25*self.estimator1(features) + 0.25*self.estimator2(features)

    def get_model(self):
        return self.estimator.get_model(), self.estimator1.get_model(), self.estimator2.get_model()

    def get_max(self):
        mean = .5*self.estimator.get_model()[0, 0] + .25*self.estimator1.get_model()[0, 0] + .25*self.estimator2.get_model()[0, 0]
        return np.max(mean)

    def reset_model(self):
        self.estimator.reset_model()
        self.estimator1.reset_model()
        self.estimator2.reset_model()

class TableEstimator(Estimator):

    def __init__(self, input_space_shape, output_space_shape, init='random'):
        self.init = init
        self.input_space_shape = input_space_shape
        self.output_space_shape = output_space_shape
        self.reset_model()

    def estimate(self, inputs):
        return self.table[tuple(inputs)]

    def get_model(self):
        return self.table

    def reset_model(self):
        if self.init == 'random':
            self.table = np.random.uniform(low=0, high=1, size=(self.input_space_shape + self.output_space_shape))
        elif self.init == 'zero':
            self.table = np.zeros(shape=(self.input_space_shape + self.output_space_shape))


class LinearEstimator(Estimator):

    def __init__(self, input_dimension, output_dimension):
        self.model = torch.nn.Linear(input_dimension, output_dimension)
        
    def estimate(self, inputs):
        tmp = torch.tensor(inputs, dtype=torch.float32)
        tmp = tmp.unsqueeze(dim=0)
        with torch.no_grad():
            outputs = self.model(tmp)
            outputs = outputs.numpy()[0]
        return outputs

    def get_model(self):
        return self.model
