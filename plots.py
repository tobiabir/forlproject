from os import walk
import numpy as np
import matplotlib.pyplot as plt

from scipy.ndimage.filters import gaussian_filter1d


filenames = next(walk('experiments_final'), (None, None, []))[2]

x_range = {}
x_range[('ber', '3x3')] = 7000
x_range[('gau1', '3x3')] = 2500
x_range[('gau2', '3x3')] = 2500
x_range[('ber', '4x4')] = 25000
x_range[('gau1', '4x4')] = 6000
x_range[('gau2', '4x4')] = 6000
x_range[('ber', '5x5')] = 50000
x_range[('gau1', '5x5')] = 15000
x_range[('gau2', '5x5')] = 15000
x_range[('ber', '6x6')] = 150000
x_range[('gau1', '6x6')] = 30000
x_range[('gau2', '6x6')] = 30000

smoothness = {}
smoothness[('ber', '3x3')] = 2
smoothness[('gau1', '3x3')] = 0.7
smoothness[('gau2', '3x3')] = 0.7
smoothness[('ber', '4x4')] = 8
smoothness[('gau1', '4x4')] = 2
smoothness[('gau2', '4x4')] = 2
smoothness[('ber', '5x5')] = 14
smoothness[('gau1', '5x5')] = 4
smoothness[('gau2', '5x5')] = 4
smoothness[('ber', '6x6')] = 50
smoothness[('gau1', '6x6')] = 12
smoothness[('gau2', '6x6')] = 12

opt_rew = {}
opt_rew['3x3'] = (4*(-1)+5)/5
opt_rew['4x4'] = (6*(-1)+5)/7
opt_rew['5x5'] = (8*(-1)+5)/9
opt_rew['6x6'] = (10*(-1)+5)/11

opt_q = {}
opt_q['3x3'] = 5*0.95**4 - sum([0.95**k for k in range(4)])
opt_q['4x4'] = 5*0.95**6 - sum([0.95**k for k in range(6)])
opt_q['5x5'] = 5*0.95**8 - sum([0.95**k for k in range(8)])
opt_q['6x6'] = 5*0.95**10 - sum([0.95**k for k in range(10)])


for gs in ('3x3', '4x4', '5x5', '6x6'):
    for rew in ('ber', 'gau1', 'gau2'):
        # plot mean rewards
        for f in sorted(filenames):
            if f.startswith(rew) and f.endswith('_mean-rewards.npy'):
                if f.endswith('sp_mean-rewards.npy') or f.endswith('Q2Q_eps_mean-rewards.npy') or ('wp' in f) or ('BQ' in f):
                    continue
                if gs not in f:
                    continue
                mean_rews = np.load('experiments_final/' + f)
                smooth = gaussian_filter1d(mean_rews, sigma=smoothness[(rew, gs)])
                plt.plot(np.arange(x_range[(rew, gs)]), smooth[:x_range[(rew, gs)]], label=f[len(gs)+len(rew)+2:-len('_eps_mean-rewards.npy')])
        plt.plot(np.arange(x_range[(rew, gs)]), opt_rew[gs]*np.ones(x_range[(rew, gs)]))
        plt.legend(loc='center right', fontsize=22)
        plt.show()


x_range[('ber', '3x3')] = 10000
x_range[('gau1', '3x3')] = 5000
x_range[('gau2', '3x3')] = 5000
x_range[('ber', '4x4')] = 25000
x_range[('gau1', '4x4')] = 15000
x_range[('gau2', '4x4')] = 15000
x_range[('ber', '5x5')] = 50000
x_range[('gau1', '5x5')] = 35000
x_range[('gau2', '5x5')] = 35000
x_range[('ber', '6x6')] = 150000
x_range[('gau1', '6x6')] = 200000
x_range[('gau2', '6x6')] = 75000


for gs in ('3x3', '4x4', '5x5', '6x6'):
    for rew in ('ber', 'gau1', 'gau2'):
        # plot max qs
        for f in sorted(filenames):
            if f.startswith(rew) and f.endswith('_max-qs.npy'):
                if f.endswith('sp_max-qs.npy') or f.endswith('Q2Q_eps_max-qs.npy') or ('wp' in f) or ('BQ' in f):
                    continue
                if gs not in f:
                    continue
                max_qs = np.load('experiments_final/' + f)

                plt.plot(np.arange(x_range[(rew, gs)]), max_qs[:x_range[(rew, gs)]], label=f[len(gs)+len(rew)+2:-len('_eps_max_qs.npy')])
                #plt.plot(np.arange(len(max_qs)), max_qs)
        plt.plot(np.arange(x_range[(rew, gs)]), opt_q[gs]*np.ones(x_range[(rew, gs)]))
        plt.legend(loc='upper right', fontsize=22)
        plt.show()
