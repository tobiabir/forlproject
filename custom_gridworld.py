import numpy as np
import random
import gym

right, up, left, down, forward = 0, 1, 2, 3, 4

def const_rew(done):
    return 5 if done else -1

def bernoulli_rew(done):
    return 5 if done else random.choice([-12, 10])

def gaussian1_rew(done):
    return 5 if done else random.gauss(-1, 5)

def gaussian2_rew(done):
    return 5 if done else random.gauss(-1, 1)

def geom_rew(done):
    return 5 if done else np.random.geometric(0.1)-11

class CustomGridworld(gym.Env):

    def __init__(self, shape=(3, 3), reward=const_rew, like_paper=True):
        self.like_paper = like_paper
        if self.like_paper:
            self.action_space = [right, up, left, down]
        else:
            self.action_space = [right, up, left, down, forward]
        self.shape = shape
        self.reset()
        self.get_reward = reward

    def reset(self):
        self.pos = (0, 0)
        self.orient = right
        return self.pos

    def step(self, action):
        done = self.pos == (self.shape[0]-1, self.shape[1]-1)

        if self.like_paper:
            if action == right and self.pos[0] < self.shape[0]-1:
                self.pos = (self.pos[0]+1, self.pos[1])
            elif action == left and self.pos[0] > 0:
                self.pos = (self.pos[0]-1, self.pos[1])
            elif action == down and self.pos[1] < self.shape[1]-1:
                self.pos = (self.pos[0], self.pos[1]+1)
            elif action == up and self.pos[1] > 0:
                self.pos = (self.pos[0], self.pos[1]-1)
        else:
            if action == forward:
                if self.orient == right and self.pos[0] < self.shape[0]-1:
                    self.pos = (self.pos[0]+1, self.pos[1])
                elif self.orient == left and self.pos[0] > 0:
                    self.pos = (self.pos[0]-1, self.pos[1])
                elif self.orient == down and self.pos[1] < self.shape[1]-1:
                    self.pos = (self.pos[0], self.pos[1]+1)
                elif self.orient == up and self.pos[1] > 0:
                    self.pos = (self.pos[0], self.pos[1]-1)
            else:
                self.orient = action

        reward = self.get_reward(done)
        if done:
            self.reset()
        return self.pos, reward, done, None

if __name__ == '__main__':
    total_sum = 0
    for i in range(100000):
        env = CustomGridworld(reward=bernoulli_rew)
        acc_rew = 0
        nobs, rew, done, _ = env.step(down)
        acc_rew += rew
        nobs, rew, done, _ = env.step(down)
        acc_rew += rew
        nobs, rew, done, _ = env.step(right)
        acc_rew += rew
        nobs, rew, done, _ = env.step(right)
        acc_rew += rew
        nobs, rew, done, _ = env.step(right)
        acc_rew += rew
        total_sum += acc_rew / 5
    print(total_sum / 100000)
