import numpy as np
import random
#import torch
import gym

import policies


def evaluate(policy, env):
    obs = env.reset()
    done = False
    acc_reward = 0
    while not done:
        action = random.randint(0,3)
        obs, reward, done, info = env.step(action)
        reward -= 1
        print(reward)
        acc_reward += reward
        env.render()
    return acc_reward


def softmax(x, temp=1):
    return np.exp(temp*x)/sum(np.exp(temp*x))


#class OARODataset(torch.utils.data.Dataset):
#
#    def __init__(self, data, n_actions, estimator, extractor, gamma):
#        self.data = data
#        self.extractor = extractor
#        self.policy = policies.EpsGreedyPolicy(n_actions, estimator, extractor, 0)
#        self.gamma = gamma
#
#    def __getitem__(self, idx):
#        observation, action, reward, observation_next = self.data[idx]
#        inputs = self.extractor(observation, action)
#        targets = reward + self.gamma * self.policy(observation_next)
#        inputs = torch.tensor(inputs, dtype=torch.float32)
#        targets = torch.tensor(targets, dtype=torch.float32).unsqueeze(dim=0)
#        return inputs, targets
#
#    def __len__(self):
#        return len(self.data)

def seed_everything(seed):
    random.seed(seed)
    np.random.seed(seed)
    torch.manual_seed(seed)
