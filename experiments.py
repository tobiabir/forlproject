from argparse import ArgumentParser
import gym
import gym_minigrid
from custom_gridworld import *
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

import estimators
import extractors
import policies
import utils
import time

import training_qlearning, training_table_double_qlearning, training_table_boltz_qlearning, training_table_qlearning, training_table_weighted_qlearning, training_table_weighted_double_qlearning, training_table_double_weighted_qlearning, training_table_q2qlearning


parser = ArgumentParser()
parser.add_argument("-nr", "--n_runs", type=int, default=5000,
                    help="number of runs to train")
parser.add_argument("-ns", "--n_steps", type=int, default=10000,
                    help="number of steps per round")
parser.add_argument("-gs", "--grid_size", type=int, default=3,
                    help="grid world side length")

parser.add_argument("-r", "--reward", type=str)
parser.add_argument("-m", "--method", type=str)
parser.add_argument("-p", "--policy", type=str)

args = parser.parse_args()

if args.grid_size == 4:
    args.n_steps = 30000
if args.grid_size == 5:
    args.n_steps = 60000
if args.grid_size == 6:
    args.n_runs = 2000
    args.n_steps = 200000

# environments = ['cartpole', 'gridword', 'mini']

rewards = [(bernoulli_rew, 'ber'),
           (gaussian1_rew, 'gau1'),
           (gaussian1_rew, 'gau2'),
           (geom_rew, 'geom')]

methods = [(training_table_qlearning.gridworld_train, 'Q'),
           (training_table_double_qlearning.gridworld_train, '2Q'),
           (training_table_weighted_qlearning.gridworld_train, 'WQ'),
           (training_table_boltz_qlearning.gridworld_train, 'BQ'),
           (training_table_weighted_double_qlearning.gridworld_train, 'W2Q'),
           (training_table_q2qlearning.gridworld_train, 'Q2Q'),
           (training_table_double_weighted_qlearning.gridworld_train, '2WQ')]

policies = [(policies.EpsGreedyPolicy, 'eps'),
            (policies.WeightedPolicy, 'wp'),
            (policies.SoftmaxPolicy, 'sp')]

n_observations, n_actions = (args.grid_size, args.grid_size), 4

for (r, r_name) in rewards:
    for (m, m_name) in methods:
        for (p, p_name) in policies:

            # if doesn't match with args
            if args.reward != r_name or args.method != m_name or args.policy != p_name:
                continue

            # exlude invalid combinations
            if p_name == 'wp' and not (m_name == 'WQ' or m_name == '2WQ'):
                continue

            env = CustomGridworld(shape=n_observations, reward=r)
            extractor = extractors.CustomLocationGridWorldExtractor()

            if m_name in ['Q2Q']:
                estimator = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                estimator1 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                estimator2 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                policy = p(n_actions, (estimator, estimator1, estimator2), extractor, estimators.Q2QEstimator)
            if m_name in ['2Q', 'W2Q', '2WQ']:
                estimator1 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                estimator2 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                policy = p(n_actions, (estimator1, estimator2), extractor, estimators.DoubleQEstimator)
            if m_name in ['Q', 'WQ', 'BQ']:
                estimator = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
                policy = p(n_actions, estimator, extractor, estimators.QEstimator)

            mean_rewards, max_qs = m(policy, env, args)
            exp_name = r_name + '_' + str(args.grid_size) + 'x' + str(args.grid_size) + '_' + m_name + '_' + p_name
            np.save('experiments/' + exp_name + '_mean-rewards', mean_rewards)
            np.save('experiments/' + exp_name + '_max-qs', max_qs)
