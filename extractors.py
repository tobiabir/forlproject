from abc import ABC, abstractmethod
import numpy as np

class Extractor(ABC):

    def __call__(self, *args, **kwargs):
        return self.extract(*args, **kwargs)
    
    @abstractmethod
    def extract(self, *args, **kwargs):
        pass

    @abstractmethod
    def get_size(self):
        pass

class CartPoleExtractor(Extractor):
    
    def extract(self, observation, action):
        action_onehot = np.zeros(2)
        action_onehot[action] = 1
        features = np.concatenate((observation, action_onehot), axis=0)
        return features

    def get_size(self):
        return 4 + 2

class CartPoleDiscreteExtractor(Extractor):

    def __init__(self, n_bins):
        self.n_bins = n_bins

    def extract(self, observation, action):
        features = self.discretize_observation(observation) + [action]
        return np.array(features)

    def discretize_observation(self, observation):
        # ugly hardcoded ranges... not sure what are good values here
        return [np.digitize(observation[0], np.linspace(-4.8, 4.8, 1))-1,
                np.digitize(observation[1], np.linspace(-0.5, 0.5, 1))-1,
                np.digitize(observation[2], np.linspace(-0.4, 0.4, 6))-1,
                np.digitize(observation[3], np.linspace(-0.9, 0.9, 3))-1]

        #return [np.digitize(observation[0], np.linspace(-2.4, 2.4, self.n_disc_steps))-1,
        #        np.digitize(observation[1], np.linspace(-3.0, 3.0, self.n_disc_steps))-1,
        #        np.digitize(observation[2], np.linspace(-0.5, 0.5, self.n_disc_steps))-1,
        #        np.digitize(observation[3], np.linspace(-2.0, 2.0, self.n_disc_steps))-1]

    def get_size(self):
        return 4 + 1

class ViewGridWorldExtractor(Extractor):
    
    def extract(self, observation, action):
        observation_flat = np.reshape(observation[0], -1)
        
        # reducing number of possible observations by only using object features
        # and only 1 for goal (8) or 0 for not goal
        f = np.vectorize(lambda x: 1 if x == 8 else 0)
        observation_flat = f(observation_flat)

        action_tmp = np.array([action])
        features = np.concatenate((observation_flat, action_tmp), axis=0)
        return features

    def get_size(self):
        return 3*3 + 1

class LocationGridWorldExtractor(Extractor):

    def extract_observation(self, observation):
        # observation features are the location of the agent which
        # has highest index (10) in object channel 
        features_observation = np.argmax(observation[:,:,0])
        return np.array([features_observation])

    def extract(self, observation, action):
        features_observation = self.extract_observation(observation)
        action_tmp = np.array([action])
        features = np.concatenate((features_observation, action_tmp), axis=0)
        return features

    def get_size(self):
        return 1 + 1

class CustomLocationGridWorldExtractor(Extractor):

    def extract_observation(self, observation):
        return observation

    def extract(self, observation, action):
        return observation + (action,)

    def get_size(self):
        return 2+1
