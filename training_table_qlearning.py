from argparse import ArgumentParser
import gym
import gym_minigrid
from custom_gridworld import *
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

import estimators
import extractors
import policies
import utils
import time


def train(policy, env, args):
    Q = policy.get_model()
    acc_rewards = []
    eps = args.epsilon
    for idx_round in tqdm(range(args.n_rounds)):
        obs = env.reset()
        done = False
        acc_reward = 0
        while not done:
            action = policy.step(obs, eps)
            obs_new, reward, done, info = env.step(action)
            acc_reward += reward

            state = extractor.extract_observation(obs)
            next_state = extractor.extract_observation(obs_new)

            # Q learning update
            max_at_next_state = np.max(Q[tuple(next_state)])
            Q[tuple(state)+(action,)] += args.alpha * (reward + args.gamma * max_at_next_state - Q[tuple(state)+(action,)])
            obs = obs_new
        acc_rewards.append(acc_reward)
        eps *= 0.9995
    return acc_rewards


# sets alpha, gamma, epsilon as in estimation paper experiments
def gridworld_train(policy, env, args, gamma=0.95):
    rewards = np.zeros((args.n_runs, args.n_steps))
    max_qs = np.zeros((args.n_runs, args.n_steps))

    for run in tqdm(range(args.n_runs)):
        policy.reset_model()
        Q = policy.get_model()
        s_count = np.zeros(env.shape)
        sa_count = np.zeros(env.shape + (policy.n_actions,))
        obs = env.reset()

        for step in range(args.n_steps):
            state = policy.extractor.extract_observation(obs)
            s_count[state] += 1
            eps = 1 / np.sqrt(s_count[state])

            action = policy.step(obs, eps)
            sa_count[state+(action,)] += 1
            alpha = 1 / sa_count[state+(action,)]**0.8

            obs_new, reward, done, info = env.step(action)
            next_state = policy.extractor.extract_observation(obs_new)
            rewards[run, step] = reward

            if not done:
                delta = reward + gamma * np.max(Q[next_state]) - Q[state+(action,)]
            else:
                delta = reward - Q[state+(action,)]
            Q[state+(action,)] += alpha * delta

            obs = obs_new
            max_qs[run, step] = policy.Q.get_max()

    return rewards.mean(axis=0), max_qs.mean(axis=0)


if __name__ == "__main__":

    parser = ArgumentParser()

    # environment arguments
    parser.add_argument("-e", "--environment", type=str, default="cartpole",
                        choices=["cartpole", "gridworld"], help="the openai gym environment")
    parser.add_argument("-g", "--gamma", type=float, default=0.9,
                        help="discount factor gamma of the MDP")

    # policy arguments
    parser.add_argument("-eps", "--epsilon", type=float, default=0.05,
                        help="epsilon for epsilon greedy strategy")
    parser.add_argument("-a", "--alpha", type=float, default=0.5,
                        help="epsilon for epsilon greedy strategy")
    parser.add_argument("-nb", "--n_bins", type=int, default=10,
                        help="number of discretization bins")

    # training arguments
    parser.add_argument("-nr", "--n_runs", type=int, default=100,
                        help="number of runs to train")
    parser.add_argument("-ns", "--n_steps", type=int, default=10000,
                        help="number of steps per round")
    parser.add_argument("-s", "--seed", type=int, default=42,
                        help="the seed")
    # parser.add_argument("-bus", "--buffer_size", type=int, default=512,
    #                     help="number of steps per buffer")

    # io arguments
    parser.add_argument("-id", "--experiment_id", type=str, default=None,
                        help="name of the experiment")

    args = parser.parse_args()

    # set seeds
    utils.seed_everything(args.seed)

    # set environment and model
    if args.environment == "cartpole":
        env = gym.make("CartPole-v0")
        n_observations, n_actions = 4, 2
        obs_shape = n_observations * (args.n_bins,)
        estimator = estimators.TableEstimator(obs_shape + (n_actions,), (1,))
        extractor = extractors.CartPoleDiscreteExtractor(args.n_bins)
    else:
        n_observations, n_actions = (3, 3), 4
        env = CustomGridworld(reward=bernoulli_rew)
        estimator = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
        extractor = extractors.CustomLocationGridWorldExtractor()
    policy = policies.EpsGreedyPolicy(n_actions, estimator, extractor)

    mean_rewards, max_qs = gridworld_train(policy, env, args)
    plt.plot(np.arange(len(mean_rewards)), mean_rewards)
    plt.plot(np.arange(len(max_qs)), max_qs)
    plt.show()
