from argparse import ArgumentParser
import gym
import gym_minigrid
from custom_gridworld import *
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

import estimators
import extractors
import policies
import utils
import time


# sets alpha, gamma, epsilon as in estimation paper experiments
def gridworld_train(policy, env, args, gamma=0.95, n_samples=200):
    rewards = np.zeros((args.n_runs, args.n_steps))
    max_qs = np.zeros((args.n_runs, args.n_steps))

    for run in tqdm(range(args.n_runs)):
        policy.reset_model()
        Q1, Q2 = policy.get_model()
        Q1_sq, Q2_sq = Q1.copy(), Q2.copy()
        sigma1, sigma2 = 1e10*np.ones(Q1.shape), 1e10*np.ones(Q2.shape)
        weights1_var, weights2_var = Q1.copy(), Q2.copy()
        probs = (1 / policy.n_actions) * np.ones(policy.n_actions)
        s_count = np.zeros(env.shape)
        sa_count_Q1 = np.zeros(env.shape + (policy.n_actions,))
        sa_count_Q2 = np.zeros(env.shape + (policy.n_actions,))
        obs = env.reset()

        for step in range(args.n_steps):
            state = policy.extractor.extract_observation(obs)
            s_count[state] += 1
            eps = 1 / np.sqrt(s_count[state])

            if type(policy) is policies.WeightedPolicy:
                action = policy.step(obs, probs)
            else:
                action = policy.step(obs, eps)

            sa_count_Q1[state+(action,)] += 1
            alpha = 1 / sa_count_Q1[state+(action,)]**0.8

            obs_new, reward, done, info = env.step(action)
            next_state = policy.extractor.extract_observation(obs_new)
            rewards[run, step] = reward

            current_sigma = sigma1[next_state]
            current_sigma[current_sigma < 1e-4] = 1e-4

            samples = np.random.normal(Q1[next_state], current_sigma, size=(policy.n_actions, n_samples))
            argmaxs = np.hstack((np.argmax(samples, axis=0), np.arange(policy.n_actions)))
            n_occs_as_max = np.bincount(argmaxs) - 1
            probs = n_occs_as_max / n_samples
            W = np.dot(probs, Q2[next_state])

            if not done:
                target = reward + gamma * W
            else:
                target = reward

            alpha = 1 / sa_count_Q1[state+(action,)]**0.8
            Q1[state+(action,)] = (1 - alpha) * Q1[state+(action,)] + alpha * target
            Q1_sq[state+(action,)] = (1 - alpha) * Q1_sq[state+(action,)] + alpha * target**2

            if sa_count_Q1[state+(action,)] > 1:
                weights1_var[state+(action,)] = (1 - alpha)**2 * weights1_var[state+(action,)] + alpha**2
                n = 1 / weights1_var[state+(action,)]
                diff = max(0, Q1_sq[state+(action,)] - Q1[state+(action,)]**2)
                sigma1[state+(action,)] = np.sqrt(diff / n);

            if random.choice([True, False]):
                Q1, Q2 = Q2, Q1
                sa_count_Q1, sa_count_Q2 = sa_count_Q2, sa_count_Q1
                Q1_sq, Q2_sq = Q2_sq, Q1_sq
                sigma1, sigma2 = sigma2, sigma1
                weights1_var, weights2_var = weights2_var, weights1_var

            obs = obs_new
            max_qs[run, step] = policy.Q.get_max()

    return rewards.mean(axis=0), max_qs.mean(axis=0)
