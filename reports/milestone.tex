% This is borrowed from the 2021 LaTeX2e package for submissions to the Conference on Neural Information Processing Systems (NeurIPS). All credits go to the authors of the original package, which was obtained from this website:
% https://nips.cc/Conferences/2021/PaperInformation/StyleFiles
% Some instructions are originated from the deep learning course in Stanford, which was obtained from this website:
% https://cs230.stanford.edu/project/

\documentclass{article}
\usepackage[final, nonatbib]{neurips_2021}
\usepackage[numbers]{natbib}
\usepackage[utf8]{inputenc} % allow utf-8 input
\usepackage[T1]{fontenc}    % use 8-bit T1 fonts
\usepackage{hyperref}       % hyperlinks
\usepackage{url}            % simple URL typesetting
\usepackage{booktabs}       % professional-quality tables
\usepackage{amsfonts}       % blackboard math symbols
\usepackage{amsmath}
\usepackage{nicefrac}       % compact symbols for 1/2, etc.
\usepackage{microtype}      % microtypography
\usepackage{graphicx}
\usepackage{xcolor}
\usepackage{algorithm}
\usepackage{algpseudocode}
\usepackage{tikz}
\usepackage{comment}

\DeclareMathOperator*{\argmax}{arg\,max}
\DeclareMathOperator*{\argmin}{arg\,min}

\title{On the Overestimation Bias of Q-learning\\--- Milestone Report ---}
% Feel free to use the same template for the Milestone

\author{
    Tobias Birchler \\
    \texttt{tobiabir@student.ethz.ch} \\
    \And
    Philip Jordan \\
    \texttt{jordanph@student.ethz.ch} \\
    % \AND
    % Author \\
    % \texttt{email@student.ethz.ch} \\
}


\begin{document}

\maketitle

\begin{comment}
\begin{abstract}
\textcolor{blue}{
The abstract should consist of 1 paragraph describing the motivation for your paper and a high-level explanation of the methodology you used/results obtained.
}
\end{abstract}
\end{comment}


\section{Introduction}
\begin{comment}
\textcolor{blue}{
Explain the problem and why it is important. Discuss your motivation for pursuing this problem. What is the main challenge of this problem? Give some background if necessary.
}
\end{comment}

Q-learning, a popular model-free method for estimating value functions of MDP policies, is known to converge to the optimal action-value function $Q^*$. However, it is also known that in many settings convergence is initially slowed down by overly optimistic estimates. The explanation is simple: for a set of random variables, taking the maximum of individual sample means is a positively biased estimator for the maximum over their expectations. Hence, designing less biased estimators is a common stepping stone towards algorithms that improve -- both in theory \cite{weng2020provably} and in practice -- on the convergence rate of Q-learning.

We collect and review existing approaches for mitigating overestimation bias in Q-learning.
As part of that we reproduce and homogenise experiments from previous work, which empirically show the overestimation bias and the effect of the approaches.
We also extend these experiments to multiple other reinforcement learning tasks.

In a second step, we aim to take on the challenge of combining and extending existing approaches as well as involving our own ideas in order to approach designing a novel variation of Q-learning that can be included in our comparisons from above.


\section{Related Work}
\begin{comment}
\textcolor{blue}{
Discuss existing papers that are related to your project. What is the state-of-the-art? Discuss their strengths and weaknesses, as well as how they are similar to and differ from your work. Google Scholar is very useful for this (you can click ``cite'' and it generates BibTeX).
}
\end{comment}

\subsection{Q-learning}

Q-learning (Algorithm~\ref{alg:q-learning}) is a famous off-policy Temporal Difference learning algorithm~\cite{sutton2018reinforcement}.

It suffers from the maximisation bias.
This is a bias introduced by the use of the Maximum Estimator (ME).
The ME estimates $\max_{i}\{\mathbb{E}[X_i]\}$ with $\max_{i}\{\mu_i\}$ for some random variables $X_i$ and their corresponding unbiased estimators $\mu_i$.
It has been established that the ME has a positive bias~\cite{smith2006optimizerscurse}.
In Q-learning the ME is used in the \texttt{action selection} and also in the \texttt{update}.

\begin{algorithm}
\caption{Q-learning}\label{alg:q-learning}
\begin{algorithmic}
\For{each step}
    \State choose action $a$ from $Q(s,:)$ $\epsilon$-greedy \Comment{action selection}
    \State take action a and observe $r$, $s'$
    \State $Q(s,a) \gets Q(s,a) + \alpha(r + \gamma\max_{a'}\{Q(s',a')\} - Q(s,a))$ \Comment{update}
    \State $s \gets s'$
\EndFor
\end{algorithmic}
\end{algorithm}

% \caption{The standard Q-learning algorithm. In the following, different variants are introduced by describing alternative instantiations of the $\triangleright$action selection and/or $\triangleright$update steps}

\subsection{Double Q-learning}

Double Q-learning~\cite{hasselt2010double} is a modification of the standard Q-learning algorithm to mitigate the maximisation bias.
In this algorithm two Q-estimates $Q_1$ and $Q_2$ are maintained.
This has the benefit that in the \texttt{update} step the maximum over the actions can be estimated by selecting the best action according to one of the estimates but the value for that action according to the other estimate.
More precisely, the \texttt{update} step then looks as follows.

\begin{algorithmic}
%\State $a_max \gets \argmax_{a}Q1(s,a)$
\State uniformly at random either $Q_u, Q_e \gets Q_1, Q_2$ or $Q_u, Q_e \gets Q_2, Q_1$
\State $Q_u(s,a) \gets Q_u(s,a) + \alpha\left(r + \gamma Q_e(s',\argmax_{a'}\{Q_u(s,a)\}) - Q_u(s,a)\right)$ \Comment{update}
\end{algorithmic}

The author shows that this results in a negatively biased estimator, which might be preferrable.

During \texttt{action selection} both estimates are first averaged before the action is selected as in Q-learning.

\begin{algorithmic}
\State choose action $a$ from $mean(Q_1,Q_2)(s,:)$ $\epsilon$-greedy \Comment{action selection}
\end{algorithmic}

\subsection{Weighted Q-learning}
The weighted estimator (WE, \cite{d2016estimating}) computes a weighted mean of sample averages with weights obtained from estimates of the probability of each variable representing the maximum. Distributions of sample means are modelled as Gaussians which is justified by the Central Limit Theorem. Weighted Q-learning then replaces the update step from Q-learning with

\begin{algorithmic}
\For{$a_i \in \mathcal A$}
\State $w_i \approx \Pr[Q(s,a_i) = \max_{j} Q(s,a_j)]$ where each $Q(s,a)$ modelled as $\mathcal{N}(\mu(s,a), \sigma(s,a)^2)$
\EndFor
\State $W \gets w^{\top} \cdot Q(s',a)$
\State $Q(s,a) \gets  Q(s,a) + \alpha [r+\gamma W-Q(s,a)]$
\State update $\mu(s,a)$ and $\sigma(s,a)$ according to sample mean and variance of observations of $Q(s,a)$
\end{algorithmic}

\begin{comment}
\section{Methods/Algorithms}
\textcolor{blue}{
The main results of your final report start from here. You can design the structure of it by yourself and divide it into different sections. Describe how you solve the problem. Describe your proposed algorithms and theoretical proofs (if any). Note: Theoretical projects may have an appendix showing extended proofs. However, TAs may not read all the details in the Appendix, so make sure you provide a high-level idea of the proofs in the main paper.
}
\end{comment}


\section{Experiments/Results/Discussion}
\begin{comment}
\textcolor{blue}{
For algorithmic and application projects, you should show the detailed configurations and experiment results. You should give details about what (hyper)parameters you chose (e.g. learning-rate and mini-batch size) and explain how and why you chose them this way. You should provide details about how you evaluate your algorithms and what metrics are used. For results, you want to have a mixture of tables and plots. You should compare your algorithm with some existing methods in the related literature. Make sure to discuss the figures/tables/comparisons in your main text. Why you think you algorithms fail/success? Why do you think that some algorithms worked better than others?
}
\end{comment}

\subsection{Environments}

\subsubsection{Mini}
The \texttt{mini} environment~\cite{sutton2018reinforcement} is probably the smallest example showing the maximisation bias.
It consists of two states $A$ and $B$.
The action space for both states is $\{\texttt{LEFT}, \texttt{RIGHT}\}$.
For action \texttt{RIGHT} in state $A$ results in a reward of $0$ and the episode ends.
For action \texttt{LEFT} the reward is also $0$ and the next state is $B$.
From $B$ both actions get reward $r\sim\mathcal{N}(-0.1,1)$ and the episode ends.

\subsubsection{Gridworld}
This environment is an instance of the classical gridworld environments.
Here we will focus on the 3x3 variant with the start state S in the upper-left corner and the target state T in the lower right corner as depicted in Figure~\ref{fig:gridworld}.
\begin{figure}[h]
\centering
\scalebox{0.7}{
\begin{tikzpicture}
\draw[step=1cm,color=gray] (0,0) grid (3,3);
\node at (+0.5,+2.5) {S};
\node at (+1.5,+2.5) {};
\node at (+2.5,+2.5) {};
\node at (+0.5,+1.5) {};
\node at (+1.5,+1.5) {};
\node at (+2.5,+1.5) {};
\node at (+0.5,+0.5) {};
\node at (+1.5,+0.5) {};
\node at (+2.5,+0.5) {T};
\end{tikzpicture}
}
\caption{Gridworld}
\label{fig:gridworld}
\end{figure}
Any action executed in state ends the episode $T$ and results in a reward of $5$.
In every other state the action is executed if possible and the reward is sampled (1) uniformly at random from $\{-10,12\}$, (2) from $\mathcal{N}(-1,5)$ or (3) from $\mathcal{N}(-1,1)$.

\subsection{Results}
For brevity we only include results from \texttt{gridworld}.
Training was conducted for 10000 steps with $\gamma = 0.95$, $\alpha = \frac{1}{N(s,a)^{0.8}}$ and $\epsilon = \frac{1}{N(s)^{0.5}}$ where $N$ is the visit count of the states and state-action pairs respectively.
We trained 500 times and average the results over these runs.
Figure~\ref{fig:plots} shows how $\max_{a}\{Q(S,a)\}$ changes over number of steps.

\begin{figure}[h]\label{fig:plots}
\caption{The value $\max_a\{Q(S,a)\}$ over number of steps. Q-learning in green, Double Q-learning in orange and Weighted Q-learning in red. In blue $\max_{a}\{Q^*(S,a)\}$. From left to right and top to bottom: Bernoulli, $\mathcal{N}(-1,5)$ and $\mathcal{N}(-1,1)$.}
\centering
\includegraphics[width=0.48\textwidth]{figs/ber_max_qs.eps}\\
\includegraphics[width=0.48\textwidth]{figs/gau1_max_qs.eps}
\includegraphics[width=0.48\textwidth]{figs/gau2_max_qs.eps}
\end{figure}

These experiments clearly show the overestimation bias of Q-learning as well as the underestimation bias of Double Q-learning, since the maximum station-action value estimates approach the true value from above, and below respectively. Weighted Q-learning further outperforms both Q- and Double-Q-learning in terms of convergence speed in all three cases.


\section{Future Work}
\begin{comment}
\textcolor{blue}{
Summarize your report and restate key points. For future work, what is the open question? What is the possible directions to improve your work?
}
\end{comment}

The weighted estimator (WE, \cite{d2016estimating}) used in weighted Q-learning builds on the observation that for increasing sample sizes, the sampling distribution of the sample mean of a set of i.i.d.\ random variables approaches a normal distribution, as stated by the central limit theorem. Comparisons with other variants of Q-learning are then carried out in a grid world environment with rewards drawn from the same Gaussian or Bernoulli distribution for all state-action pairs. It is then concluded that weighted Q-learning is effective even for non-Gaussian reward distributions. However, this raises the following question:
    
A sum of i.i.d.\ Bernoulli random variables follows a binomial distribution which is essential a discrete version of a Normal distribution, i.e. very close to Normal even for small sample sizes. Hence this choice yields friendly conditions for the WE. Does weighted Q-learning still perform better than its competitors when choosing other reward distributions, in particular, ones with slow convergence in the setting of the central limit theorem?

\begin{comment}
\section{Contributions}
\textcolor{blue}{
You can include a section that describes what each team member worked on and contributed to the project. This is not included in the 8 page limit.
}
\end{comment}


\medskip
\bibliographystyle{unsrtnat}
\bibliography{ref.bib}

\begin{comment}
% Please remember to delete this part for instructions on the references.
\textcolor{blue}{
At the end of your paper, you should include citations for: (1) Any papers mentioned in the related work section. (2) Papers describing algorithms that you used. (3) Code or libraries you used. The references section is excluded from the 8 page limit. If you are using TeX, you can use any bibliography format and citation style as long as it is consistent. We provide an example using \texttt{natbib} to cite one of the most famous books in reinforcement learning \cite{sutton2018reinforcement}.
}
\end{comment}


\clearpage
\begin{comment}
\appendix
\section{Appendix}
\textcolor{blue}{
You can defer the lengthy proofs to the appendix (if any). This is not included in the 8 page limit.
}
\end{comment}

\end{document}
