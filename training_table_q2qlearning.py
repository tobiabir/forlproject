from argparse import ArgumentParser
import gym
import gym_minigrid
from custom_gridworld import *
import os
from tqdm import tqdm
import numpy as np
import matplotlib.pyplot as plt

import estimators
import extractors
import policies
import utils
import utils_training
import time


def train(policy, env, args):
    Q1, Q2 = policy.get_model()
    acc_rewards = []
    eps = args.epsilon
    alpha = args.alpha
    for idx_round in tqdm(range(args.n_rounds)):
        obs = env.reset()
        done = False
        acc_reward = 0
        while not done:
            action = policy.step(obs, eps)
            obs_new, reward, done, info = env.step(action)
            acc_reward += reward

            state = extractor.discretize_observation(obs)
            next_state = extractor.discretize_observation(obs_new)

            # Q learning update
            max_at_next_state = np.max(Q2[tuple(next_state)])
            Q1[tuple(state)+(action,)] += alpha * (reward + args.gamma * max_at_next_state - Q1[tuple(state)+(action,)])
            Q1, Q2 = Q2, Q1
            obs = obs_new

        acc_rewards.append(acc_reward)
        eps *= 0.9995
        alpha *= 0.99995
    return acc_rewards


# sets alpha, gamma, epsilon as in estimation paper experiments
def gridworld_train(policy, env, args, gamma=0.95):
    rewards = np.zeros((args.n_runs, args.n_steps))
    max_qs = np.zeros((args.n_runs, args.n_steps))

    for run in tqdm(range(args.n_runs)):
        policy.reset_model()
        Q, Q1, Q2 = policy.get_model()
        s_count = np.zeros(env.shape)
        sa_count_Q = np.zeros(env.shape + (policy.n_actions,))
        sa_count_Q1 = np.zeros(env.shape + (policy.n_actions,))
        sa_count_Q2 = np.zeros(env.shape + (policy.n_actions,))
        obs = env.reset()

        for step in range(args.n_steps):
            state = policy.extractor.extract_observation(obs)
            s_count[state] += 1
            eps = 1 / np.sqrt(s_count[state])

            action = policy.step(obs, eps)
            sa_count_Q1[state+(action,)] += 1
            sa_count_Q[state+(action,)] += 1
            alpha1 = 1 / sa_count_Q1[state+(action,)]**0.8
            alpha = 1 / sa_count_Q[state+(action,)]**0.8

            obs_new, reward, done, info = env.step(action)
            next_state = policy.extractor.extract_observation(obs_new)
            rewards[run, step] = reward

            max_action = np.argmax(Q1[next_state])
            if not done:
                delta1 = reward + gamma * Q2[next_state+(max_action,)] - Q1[state+(action,)]
                delta = reward + gamma * np.max(Q[next_state]) - Q[state+(action,)]
            else:
                delta1 = reward - Q1[state+(action,)]
                delta = reward - Q[state+(action,)]
            Q1[state+(action,)] += alpha1 * delta1
            Q[state+(action,)] += alpha * delta

            if random.choice([True, False]):
                Q1, Q2 = Q2, Q1
                sa_count_Q1, sa_count_Q2 = sa_count_Q2, sa_count_Q1

            obs = obs_new
            max_qs[run, step] = policy.Q.get_max()

    return rewards.mean(axis=0), max_qs.mean(axis=0)


if __name__ == "__main__":

    parser = ArgumentParser(parents=[utils_training.training_parser])

    # environment arguments
    parser.add_argument("-e", "--environment", type=str, default="CartPole-v0",
                        help="the openai gym environment")
    parser.add_argument("-g", "--gamma", type=float, default=0.9,
                        help="discount factor gamma of the MDP")

    # policy arguments
    parser.add_argument("-eps", "--epsilon", type=float, default=0.05,
                        help="epsilon for epsilon greedy strategy")
    parser.add_argument("-a", "--alpha", type=float, default=0.5,
                        help="epsilon for epsilon greedy strategy")
    parser.add_argument("-nb", "--n_bins", type=int, default=10,
                        help="number of discretization bins")

    # training arguments
    parser.add_argument("-nr", "--n_runs", type=int, default=100,
                        help="number of runs to train")
    parser.add_argument("-ns", "--n_steps", type=int, default=10000,
                        help="number of steps per round")

    # io arguments
    parser.add_argument("-id", "--experiment_id", type=str, default=None,
                        help="name of the experiment")

    args = parser.parse_args()


    # set environment and model
    if args.environment == "cartpole":
        env = gym.make("CartPole-v0")
        n_observations, n_actions = 4, 2
        obs_shape = n_observations * (args.n_bins,)
        estimator1 = estimators.TableEstimator(obs_shape + (n_actions,), (1,))
        estimator2 = estimators.TableEstimator(obs_shape + (n_actions,), (1,))
        extractor = extractors.CartPoleDiscreteExtractor(args.n_bins)
    else:
        n_observations, n_actions = (3, 3), 4
        env = CustomGridworld(reward=bernoulli_rew)
        estimator = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
        estimator1 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
        estimator2 = estimators.TableEstimator(n_observations+(n_actions,), (1,), init='zero')
        extractor = extractors.CustomLocationGridWorldExtractor()

    policy = policies.EpsGreedyPolicy(n_actions, (estimator, estimator1, estimator2), extractor, estimators.Q2QEstimator)

    mean_rewards, max_qs = gridworld_train(policy, policy0, env, args)
    plt.plot(np.arange(len(mean_rewards)), mean_rewards)
    plt.plot(np.arange(len(max_qs)), max_qs)
    plt.show()

#    while(True):
#    print(utils.evaluate(policy, env))
#    time.sleep(1)
