from argparse import ArgumentParser
import os
from tqdm import tqdm

training_parser = ArgumentParser(add_help=False)

# training arguments
training_parser.add_argument("-nw", "--n_workers", type=int, default=os.cpu_count(),
                    help="number of workers to use")
training_parser.add_argument("-ne", "--n_epochs", type=int, default=4,
                    help="number of epochs to train in a training round")
training_parser.add_argument("-bs", "--batch_size", type=int, default=32,
                    help="size of batches for training")
training_parser.add_argument("-as", "--accumulation_size", type=int, default=1,
                    help="number of batches to accumulate (gradient accumulation)")
training_parser.add_argument("-mp", "--mixed_precision", default=False, action="store_true",
                    help="set to enable mixed precision training")
training_parser.add_argument("-d", "--device", type=str, default="cpu",
                    help="device to use for training")

# io arguments
training_parser.add_argument("-pc", "--path_checkpoint_in", type=str, default=None,
                    help="directory to write checkpoints to")
training_parser.add_argument("-dcs", "--dir_checkpoint_step", type=str, default=None,
                    help="directory to write checkpoints to")
training_parser.add_argument("-ics", "--interval_checkpoint_step", type=int, default=None,
                    help="interval for writing checkpoints")
training_parser.add_argument("-dce", "--dir_checkpoint_epoch", type=str, default=None,
                    help="directory to write checkpoints to")
training_parser.add_argument("-v", "--verbose", default=False, action="store_true",
                    help="set to get verbose output")


def move_to_device(x, device):
    """ move torch tensors in x to specified torch device

    Args:
        x: datastructure containing torch tensors
        device : torch device to move to

    Returns:
        same datastructure as x but with torch tensors moved to specified device
    """
    if torch.is_tensor(x):
        x = x.to(device)
    elif isinstance(x, dict):
        for key in x:
            x[key] = move_to_device(x[key], device)
    else:
        for idx in range(len(x)):
            x[idx] = move_to_device(x[idx], device)
    return x


def train_model(model, dataloader, fn_loss, args):
    """ train the model and save checkpoints after every epoch

    Args:
        model : model to train
        dataloader : data to train on
        fn_loss: loss function to use
        args: command line arguments

    Returns:
        None
    """
    model.train()
    model.to(args.device)
    optimizer = torch.optim.Adam(model.parameters(), lr=2e-5)
    scaler = torch.cuda.amp.GradScaler(enabled=args.mixed_precision)
    for idx_epoch in range(args.n_epochs):
        model.train()
        avg_loss = 0.0
        if hasattr(dataloader.dataset, "__len__"):
            total = len(dataloader) // args.accumulation_size
        else:
            total = None
        #pbar = tqdm(total=total, unit="batch",
        #            desc=f"Epoch {idx_epoch}/{args.n_epochs}")
        for i, batch in enumerate(dataloader):
            batch = move_to_device(batch, args.device)
            inputs, labels = batch
            with torch.cuda.amp.autocast(enabled=args.mixed_precision):
                preds = model(inputs)
                loss = fn_loss(preds, labels)
                loss /= args.accumulation_size
            scaler.scale(loss).backward()
            avg_loss += loss.item()
            if (i + 1) % args.accumulation_size == 0:
                scaler.step(optimizer)
                scaler.update()
                optimizer.zero_grad()
                #pbar.set_postfix(loss=avg_loss)
                #pbar.update(1)
                avg_loss = 0.0
                idx_step = i // args.accumulation_size
                if args.dir_checkpoint_step is not None and (idx_step + 1) % args.interval_checkpoint_step == 0:
                    checkpoint = {
                        "step": idx_step + 1,
                        "model_state_dict": model.state_dict(),
                    }
                    path_checkpoint = os.path.join(
                        args.dir_checkpoint_step, f"checkpoint_{idx_step+1}")
                    torch.save(checkpoint, path_checkpoint)
        if args.dir_checkpoint_epoch is not None:
            checkpoint = {
                "epoch": idx_epoch + 1,
                "model_state_dict": model.state_dict(),
            }
            path_checkpoint = os.path.join(
                args.dir_checkpoint_epoch, f"checkpoint_{idx_epoch+1}")
            torch.save(checkpoint, path_checkpoint)
    model.to("cpu")
