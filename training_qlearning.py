from argparse import ArgumentParser
import gym
from matplotlib import pyplot as plt
import numpy as np
import os
from tqdm import tqdm

import estimators
import extractors
import policies
import utils
import utils_training


def run_episode(policy, env, args):
    buff = []
    reward_sum = 0
    observation = env.reset()
    done = False
    while not done:
        action = policy.step(observation)
        observation_new, reward, done, info = env.step(action)
        buff.append((observation, action, reward, observation_new))
        reward_sum += reward
        observation = observation_new
    return buff, reward_sum


def train(policy, env, args):
    buff = []
    rewards = []
    for idx_round in tqdm(range(args.n_rounds)):
        episode_buffer, reward = run_episode(policy, env, args)
        buff += episode_buffer   # shouldn't the buffer be reset after every episode?
        rewards.append(reward)
        while(len(buff) > args.buffer_size):
            buff.pop(0)
        dataset = utils.OARODataset(buff, policy.n_actions, policy.Q.estimator, policy.Q.extractor, args.gamma)
        fn_loss = torch.nn.MSELoss()
        dataloader = torch.utils.data.DataLoader(
            dataset=dataset, batch_size=args.batch_size, shuffle=True, num_workers=4)
        utils_training.train_model(policy.get_model(), dataloader, fn_loss, args)

        # saving checkpoint
        if args.dir_checkpoint_round is not None and (idx_round + 1) % args.interval_checkpoint_round == 0:
            checkpoint = {
                "round": idx_round + 1,
                "model_state_dict": policy.get_model().state_dict(),
            }
            path_checkpoint = os.path.join(
                args.dir_checkpoint_round, f"checkpoint_{idx_round+1}")
            torch.save(checkpoint, path_checkpoint)
         
    return rewards


if __name__ == "__main__":

    parser = ArgumentParser(parents=[utils_training.training_parser])

    # environment arguments
    parser.add_argument("--seed", type=int, default=1,
                        help="fix random seeds")
    parser.add_argument("-e", "--environment", type=str, default="CartPole-v0",
                        help="the openai gym environment")
    parser.add_argument("-g", "--gamma", type=float, default=0.9,
                        help="discount factor gamma of the MDP")

    # policy arguments
    parser.add_argument("-eps", "--epsilon", type=float, default=0.05,
                        help="epsilon for epsilon greedy strategy")

    # training arguments
    parser.add_argument("-nr", "--n_rounds", type=int, default=4,
                        help="number of rounds to train")
    parser.add_argument("-bus", "--buffer_size", type=int, default=512,
                        help="number of steps per buffer")

    # io arguments
    parser.add_argument("-id", "--experiment_id", type=str, default=None,
                        help="name of the experiment")
    parser.add_argument("-dcr", "--dir_checkpoint_round", type=str, default=None,
                        help="directory to write checkpoints to")
    parser.add_argument("-icr", "--interval_checkpoint_round", type=int, default=None,
                        help="interval for writing checkpoints")

    args = parser.parse_args()

    if args.dir_checkpoint_step is not None:
        os.makedirs(args.dir_checkpoint_step, exist_ok=True)
    if args.dir_checkpoint_epoch is not None:
        os.makedirs(args.dir_checkpoint_epoch, exist_ok=True)

    utils.seed_everything(args.seed)
    env = gym.make("CartPole-v0")
    n_observations = 4
    n_actions = 2 
    n_features = n_observations + n_actions
    estimator = estimators.LinearEstimator(n_features, 1)
    extractor = extractors.CartPoleExtractor()
    policy = policies.EpsGreedyPolicy(n_actions, estimator, extractor, args.epsilon)

    rewards = train(policy, env, args)

    x = np.arange(len(rewards))
    y = np.array(rewards)
    plt.plot(x,y)
    plt.show()

    print("\n", utils.evaluate(policy, args.environment))
