from abc import ABC, abstractmethod
import numpy as np
import random
import utils

import estimators
#import utils_space


class Policy(ABC):

    @abstractmethod
    def step(self, observation):
        pass

    def __call__(self, observation):
        return self.step(observation)


class RandomPolicy(Policy):

    def __init__(self, action_space):
        self.action_space = action_space

    def step(self, observation):
        return self.action_space.sample()


class EpsGreedyPolicy(Policy):

    def __init__(self, n_actions, estimator, extractor, q_estimator=estimators.QEstimator):
        self.n_actions = n_actions
        self.extractor = extractor
        self.Q = q_estimator(estimator, extractor)

    def step(self, observation, eps):
        if random.random() < eps:
            return random.choice(range(self.n_actions))
        Qs = np.zeros(self.n_actions)
        for action in range(self.n_actions):
            Qs[action] = self.Q(observation, action)
        return np.argmax(Qs)

    def get_model(self):
        return self.Q.get_model()

    def reset_model(self):
        self.Q.reset_model()


class WeightedPolicy(Policy):

    def __init__(self, n_actions, estimator, extractor, q_estimator=estimators.QEstimator):
        self.n_actions = n_actions
        self.extractor = extractor
        self.Q = q_estimator(estimator, extractor)

    def step(self, observation, prob_weights):
        return np.random.choice(range(self.n_actions), 1, p=prob_weights)

    def get_model(self):
        return self.Q.get_model()

    def reset_model(self):
        self.Q.reset_model()


class SoftmaxPolicy(Policy):

    def __init__(self, n_actions, estimator, extractor, q_estimator=estimators.QEstimator):
        self.n_actions = n_actions
        self.extractor = extractor
        self.Q = q_estimator(estimator, extractor)

    def step(self, observation, eps):
        Qs = np.zeros(self.n_actions)
        for action in range(self.n_actions):
            Qs[action] = self.Q(observation, action)
        return np.random.choice(range(self.n_actions), p=utils.softmax(Qs, 1))

    def get_model(self):
        return self.Q.get_model()

    def reset_model(self):
        self.Q.reset_model()

#    def update_table(self, state, action, reward, next_state, alpha, gamma):
#        self.Q.estimator.update(state, action, reward, next_state, alpha, gamma)
