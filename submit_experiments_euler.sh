module load python
source .venv/bin/activate

for N in 4 5 6; do
        for REW in ber gau1 gau2; do
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m Q -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m 2Q -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m WQ -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m BQ -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m W2Q -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m 2WQ -p eps
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m Q2Q -p eps

                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m WQ -p wp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m 2WQ -p wp

                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m Q -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m 2Q -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m WQ -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m BQ -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m W2Q -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m 2WQ -p sp
                bsub -W 24:00 -o "$(pwd)/logs.txt" -n 4 -R "rusage[mem=2048]" python experiments.py -r $REW -gs $N -m Q2Q -p sp
        done
done
